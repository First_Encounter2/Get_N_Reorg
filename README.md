<body bgcolor="#c0c0c0">
<h1 align="center"><a href="https://codeberg.org/First_Encounter2/Get_N_Reorg/releases">GetNReorg</a></h1>
<p>Используется для преобразования сырого JSON в <i>"удобочитаемый"</i>, а также получения документов из облака Эвотор.</p>
<p>В большинстве случаев достаточно вставить JSON в поле для ввода и нажать Save as.
    Далее выберите путь сохранения и имя файла.</p>
<p>Если необходимо запросить документ из облака Эвотор, то
    в соответствующее поле вставьте token от приложения. 
    Возможности token уточняйте в документации на приложение, от которого используете token.
</p>
<h4>Кнопки (основное окно):</h4>
<ul>
    <li><b>GET</b> - для запроса в облако Эвотор.
        Для осуществления гет-запроса должны быть заполнены соответстующие поля в виджетах.</li>
    <li><b>PASTE</b> - для вставки из буфера обмена</li>
    <li><b>SAVE AS</b> - для сохранения содержимого поля в файл</li>
    <li><b>CLEAR</b> - для очистки заполненного поля</li>
</ul>
<h4>Кнопки (дочернее окно):</h4>
<ul>
    <li><b>OK</b> - отправить запрос с указанными параметрами</li>
    <li><b>⭮</b> - вставить данные из предыдущего запроса</li>
    <li><b>Закрыть</b> - закрыть окно парметров запроса, не осуществляя запрос</li>
</ul>

</body>
