"""Child window creation module with request parameters

После создания дочернего окна,
при помощи виджетов можно задать нужные параметры запроса.
В результате работы, будет получен отформатированный документ JSON,
который автоматически будет вставлен
в виджет работы с текстом для последующего сохранения.
"""
import tkinter as tk
import tkinter.messagebox as mb
from tkinter import ttk, X
from datetime import datetime, timedelta
from functools import partial
import sys
import json
import os
import requests
from hovers_and_tooltips import hat


def get_stores(token, label_for_text):
    """Getting a list of stores from the cloud.

    При помощи функции get_stores
    осуществляется запрос на построение списка торговых точек.

    :return: переработанный список"""
    url_for_get_stores = "https://api.evotor.ru/api/v1/inventories/stores/search"
    headers = {"X-Authorization": token}
    try:
        request = requests.get(url_for_get_stores, headers=headers)
        request.raise_for_status()
        raw_dict = json.loads(request.text)
        screened_list = []
        for i in raw_dict:
            store_uuid = i["uuid"]
            name = i["name"]
            screened_list.append(f"{name}, {store_uuid}")
        return screened_list
    except requests.exceptions.HTTPError:
        mb.showinfo("HTTP Error", str(sys.exc_info()[1]))
        label_for_text.configure(text="Error", fg="red")
    return [""]


class Clndr(tk.Toplevel):
    """The class that initiates the position
    of the child window's widgets
    and the generation of the request

    Данный класс необходим для того, чтобы задать нужные параметры запроса,
    а также расположить виджеты будущего дочернего окна.
    """
    # pylint: disable=too-many-instance-attributes
    main_bg = "#282c34"
    main_fg = "#FFFFFF"
    main_widgets_width = 70
    style = "ridge"
    BLACK_LIGHT = '#3e4451'

    doc_types = [
        "ACCEPT",
        "INVENTORY",
        "REVALUATION",
        "RETURN",
        "WRITE_OFF",
        "SELL",
        "PAYBACK",
        "BUY",
        "BUYBACK",
        "OPEN_TARE",
        "OPEN_SESSION",
        "CLOSE_SESSION",
        "POS_OPEN_SESSION",
        "CASH_INCOME",
        "CASH_OUTCOME",
        "X_REPORT",
        "Z_REPORT",
        "CORRECTION",
    ]

    def apply(self, token, text_widget, label_for_text):
        """Collecting all specified parameters in the request"""
        doc_type = self.combobox_doc_type.get()
        store_uuid = self.combobox_stores.get().split(",")
        try:
            since_dt = datetime.strptime(
                self.entry_time_since.get(), "%Y-%m-%d %H:%M:%S"
            )
            until_dt = datetime.strptime(
                self.entry_time_until.get(), "%Y-%m-%d %H:%M:%S"
            )
            since_uts = int(since_dt.timestamp()) * 1000
            until_uts = int(until_dt.timestamp()) * 1000
            url_for_docs = (
                f"https://api.evotor.ru/stores/{store_uuid[1].strip()}/documents"
            )
            headers = {"X-Authorization": token}
            params = {"type": doc_type, "since": since_uts, "until": until_uts}
            try:
                request = requests.get(url_for_docs, headers=headers, params=params)
                print(request.status_code)
                request.raise_for_status()
                with open("temp_file", "w", encoding="utf-8") as temp_file:
                    reorg_json = json.loads(request.text)
                    json.dump(reorg_json, temp_file, ensure_ascii=False, indent=4)
                with open("temp_file", "r", encoding="utf-8") as temp_file:
                    text_widget.insert(1.0, temp_file.read())
                    label_for_text.configure(
                        text="Received. Don't forget to save!", fg="#ffbf00"
                    )
                os.remove("temp_file")
                with open("metadate_file", "w", encoding="utf-8") as metadate_file:
                    metadate_file.write(
                        f"{self.entry_time_since.get()},{self.entry_time_until.get()},{token},"
                        f"{self.combobox_doc_type.current()}"
                    )
                self.destroy()
            except requests.exceptions.HTTPError:
                mb.showinfo("HTTP Error", str(sys.exc_info()[1]))
                self.label_notifications.configure(text="Error!", fg="red")
                self.after(
                    2000,
                    lambda: self.label_notifications.configure(
                        text="Проверьте введенные данные, в т.ч указанное время"
                    ),
                )
        except ValueError:
            mb.showinfo("Нарушен формат времени/даты", str(sys.exc_info()[1]))
            self.label_notifications.configure(text="Error!", fg="red")
            self.after(
                2000,
                lambda: self.label_notifications.configure(
                    text="После исправления формата укажите параметры запроса и нажмите Ок"
                ),
            )

    def time_handler(self):
        """The function substitutes the time template"""
        str_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.entry_time_until.insert(0, str_datetime)
        self.entry_time_since.insert(0, str_datetime)

    def session_recovery_assistant(self, token):
        """The function allows you to return the last values of a successful request"""
        meta_date = os.path.exists("metadate_file")
        if meta_date:
            with open("metadate_file", "r", encoding="utf-8") as metadate_file:
                date_string = metadate_file.read()
                list_of_dates = list(date_string.split(","))
            self.entry_time_since.delete(0, tk.END)
            self.entry_time_until.delete(0, tk.END)
            self.entry_time_since.insert(0, list_of_dates[0])
            self.entry_time_until.insert(0, list_of_dates[1])
            self.combobox_doc_type.current(int(list_of_dates[3]))
        if not meta_date:
            self.entry_time_since.delete(0, tk.END)
            self.entry_time_until.delete(0, tk.END)
            self.time_handler()
            with open("metadate_file", "w", encoding="utf-8") as metadate_file:
                metadate_file.write(
                    f"{self.entry_time_since.get()},{self.entry_time_until.get()},{token},"
                    f"{self.combobox_doc_type.current()}"
                )

    def quick_patterns_today(self):
        today_date = datetime.now().strftime("%Y-%m-%d ")
        since_time = today_date + "00:00:00"
        until_time = today_date + "23:59:59"

        self.entry_time_since.delete(0, tk.END)
        self.entry_time_until.delete(0, tk.END)

        self.entry_time_since.insert(0, since_time)
        self.entry_time_until.insert(0, until_time)

    def quick_patterns_yesterday(self):
        today_date = datetime.now() - timedelta(days=1)
        yesterday_date = today_date.strftime("%Y-%m-%d ")
        since_time = yesterday_date + "00:00:00"
        until_time = yesterday_date + "23:59:59"

        self.entry_time_since.delete(0, tk.END)
        self.entry_time_until.delete(0, tk.END)

        self.entry_time_since.insert(0, since_time)
        self.entry_time_until.insert(0, until_time)

    def __init__(self, parent, token, text_widget, label_for_text):
        super().__init__(parent)
        self.geometry("450x340+200+200")
        self["bg"] = self.main_bg
        self.title("Параметры запроса")

        # doc

        self.label_frame_doc = tk.LabelFrame(
            self,
            text="Тип документа",
            bg=self.main_bg,
            fg=self.main_fg,
            borderwidth=0,
            width=1,
            font="sans-serif 10",
        )
        self.label_frame_doc.pack(pady=5)

        self.combobox_doc_type = ttk.Combobox(
            self.label_frame_doc,
            values=self.doc_types,
            state="readonly",
            width=self.main_widgets_width,
        )
        self.combobox_doc_type.current(5)
        self.combobox_doc_type.pack()

        # store

        self.label_frame_store = tk.LabelFrame(
            self,
            text="Торговая точка",
            bg=self.main_bg,
            fg=self.main_fg,
            borderwidth=0,
            width=1,
            font="sans-serif 10",
        )
        self.label_frame_store.pack(pady=5)

        self.combobox_stores = ttk.Combobox(
            self.label_frame_store,
            values=get_stores(token, label_for_text),
            state="readonly",
            width=self.main_widgets_width,
        )
        self.combobox_stores.current(0)
        self.combobox_stores.pack()

        # quick patterns

        self.quick_patterns_frame = tk.Frame(
            self,
            bg=self.main_bg
        )
        self.quick_patterns_frame.pack(fill=X, pady=5)

        self.quick_patterns_label = tk.Label(
            self.quick_patterns_frame,
            text="Документы за:",
            font="sans-serif 10",
            bg=self.main_bg,
            fg=self.main_fg,
        )

        self.quick_patterns_label.pack(side='left', padx=3)

        # since

        self.label_frame_time_since = tk.LabelFrame(
            self,
            text="С даты и времени",
            bg=self.main_bg,
            fg=self.main_fg,
            borderwidth=0,
            width=1,
            font="sans-serif 10",
        )
        self.label_frame_time_since.pack(pady=5)

        self.entry_time_since = tk.Entry(
            self.label_frame_time_since,
            bg=self.main_fg,
            fg=self.main_bg,
            width=self.main_widgets_width + 3,
        )
        self.entry_time_since.pack()

        self.template_reminder_label = tk.Label(
            self.label_frame_time_since,
            text="Year-Month-Day Hour:Minute:Second",
            font="sans-serif 7",
            bg=self.main_bg,
            fg=self.main_fg,
        )
        self.template_reminder_label.pack(side="left")

        # until

        self.label_frame_time_until = tk.LabelFrame(
            self,
            text="По дату и время",
            bg=self.main_bg,
            fg=self.main_fg,
            borderwidth=0,
            width=1,
            font="sans-serif 10",
        )
        self.label_frame_time_until.pack(pady=5)

        self.entry_time_until = tk.Entry(
            self.label_frame_time_until,
            bg=self.main_fg,
            fg=self.main_bg,
            width=self.main_widgets_width + 3,
        )
        self.entry_time_until.pack()

        self.template_reminder_label = tk.Label(
            self.label_frame_time_until,
            text="Year-Month-Day Hour:Minute:Second",
            font="sans-serif 7",
            bg=self.main_bg,
            fg=self.main_fg,
        )
        self.template_reminder_label.pack(side="left")

        # reminder

        self.label_notifications = tk.Label(
            self,
            text="Укажите параметры запроса и нажмите Ок",
            font="sans-serif 7",
            bg=self.main_bg,
            fg=self.main_fg,
        )
        self.label_notifications.pack()

        # buttons

        self.frame_button = tk.Frame(self, bg=self.main_bg)
        self.frame_button.pack(pady=15)

        self.button_apply = tk.Button(
            self.frame_button,
            text="Ок",
            bg=self.main_bg,
            font="sans-serif 10",
            command=partial(self.apply, token, text_widget, label_for_text),
            fg=self.main_fg,
            relief=self.style,
            activebackground=self.main_bg,
            activeforeground=self.main_fg
        )
        self.button_apply.pack(side="left")

        self.tooltip_button_apply_enter = hat.Hover(
            'Выполнить запрос',
            self.label_notifications,
            self.BLACK_LIGHT,
            self.main_fg
        )

        self.tooltip_button_apply_leave = hat.Hover(
            'Укажите параметры запроса и нажмите Ок',
            self.label_notifications,
            self.main_bg,
            self.main_fg
        )

        self.button_apply.bind('<Enter>', self.tooltip_button_apply_enter.hover_on)
        self.button_apply.bind('<Leave>', self.tooltip_button_apply_leave.hover_off)

        self.label_date_selection_notice = tk.Label(
            self, text="", bg=self.main_bg, fg="#00FF00", font="sans-serif 7"
        )

        self.button_restore_session = tk.Button(
            self.frame_button,
            text="⭮",
            bg=self.main_bg,
            font="sans-serif 10",
            command=partial(self.session_recovery_assistant, token),
            fg=self.main_fg,
            relief=self.style,
            activebackground=self.main_bg,
            activeforeground=self.main_fg
        )
        self.button_restore_session.pack(side="left")

        self.tooltip_button_restore_session_enter = hat.Hover(
            'Восстановить данные последнего успешного запроса',
            self.label_notifications,
            self.BLACK_LIGHT,
            self.main_fg
        )

        self.tooltip_button_restore_session_leave = hat.Hover(
            'Укажите параметры запроса и нажмите Ок',
            self.label_notifications,
            self.main_bg,
            self.main_fg
        )

        self.button_restore_session.bind('<Enter>', self.tooltip_button_restore_session_enter.hover_on)
        self.button_restore_session.bind('<Leave>', self.tooltip_button_restore_session_leave.hover_off)

        self.button_cancel = tk.Button(
            self.frame_button,
            text="Закрыть",
            command=self.destroy,
            bg=self.main_bg,
            font="sans-serif 10",
            fg=self.main_fg,
            relief=self.style,
            activebackground=self.main_bg,
            activeforeground=self.main_fg
        )
        self.button_cancel.pack(side="left")

        self.tooltip_button_cancel_enter = hat.Hover(
            'Закрыть окно',
            self.label_notifications,
            self.BLACK_LIGHT,
            self.main_fg
        )

        self.tooltip_button_cancel_leave = hat.Hover(
            'Укажите параметры запроса и нажмите Ок',
            self.label_notifications,
            self.main_bg,
            self.main_fg
        )

        self.button_cancel.bind('<Enter>', self.tooltip_button_cancel_enter.hover_on)
        self.button_cancel.bind('<Leave>', self.tooltip_button_cancel_leave.hover_off)

        self.button_today = tk.Button(
            self.quick_patterns_frame,
            text="Сегодня",
            bg=self.main_bg,
            font="sans-serif 8",
            command=self.quick_patterns_today,
            fg=self.main_fg,
            relief=self.style,
            activebackground=self.main_bg,
            activeforeground=self.main_fg
        )

        self.button_today.pack(side='left')

        self.tooltip_button_today_enter = hat.Hover(
            'Установить дату за сегодняшние сутки',
            self.label_notifications,
            self.BLACK_LIGHT,
            self.main_fg
        )

        self.tooltip_button_today_leave = hat.Hover(
            'Укажите параметры запроса и нажмите Ок',
            self.label_notifications,
            self.main_bg,
            self.main_fg
        )

        self.button_today.bind('<Enter>', self.tooltip_button_today_enter.hover_on)
        self.button_today.bind('<Leave>', self.tooltip_button_today_leave.hover_off)

        self.button_yesterday = tk.Button(
            self.quick_patterns_frame,
            text="Вчера",
            bg=self.main_bg,
            font="sans-serif 8",
            command=self.quick_patterns_yesterday,
            fg=self.main_fg,
            relief=self.style,
            activebackground=self.main_bg,
            activeforeground=self.main_fg
        )

        self.button_yesterday.pack(side='left')

        self.tooltip_button_yesterday_enter = hat.Hover(
            'Установить дату за вчерашние сутки',
            self.label_notifications,
            self.BLACK_LIGHT,
            self.main_fg
        )

        self.tooltip_button_yesterday_leave = hat.Hover(
            'Укажите параметры запроса и нажмите Ок',
            self.label_notifications,
            self.main_bg,
            self.main_fg
        )

        self.button_yesterday.bind('<Enter>', self.tooltip_button_yesterday_enter.hover_on)
        self.button_yesterday.bind('<Leave>', self.tooltip_button_yesterday_leave.hover_off)
