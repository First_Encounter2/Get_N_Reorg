#!/usr/bin/python3
"""Главный модуль для преобразования json в удобный для чтения вид"""

import tkinter as tk
import tkinter.messagebox as mb
from tkinter import (
    filedialog,
    Tk,
    Label,
    LabelFrame,
    LEFT,
    RIGHT,
    BOTTOM,
    Y,
    X,
    Entry,
    Scrollbar,
    BOTH,
    HORIZONTAL,
    Button,
)
import json
import os
import sys
import child_query.clndr
from hovers_and_tooltips import hat
from search import search_func

# main window options

MAIN_BG = "#282c34"
MAIN_FG = "#FFFFFF"
BLACK_LIGHT = '#3e4451'

window = Tk()
window.title("GetNReorg")
window["bg"] = MAIN_BG

if os.name == "nt":
    ICON_EXISTS = os.path.exists("ico/cloud_16.png")
    if ICON_EXISTS:
        window.iconphoto(True, tk.PhotoImage(file="ico/cloud_16.png"))
    else:
        ICON = None
else:
    ICON = None

# constants

VERSION = "1.5.9"
STYLE = "ridge"


# funcs


def cooked_json():
    """Formatting the JSON and saving it to a file

    Функция форматирует JSON и позволяет выбрать имя файла и путь,
    куда требуется сохранить данный файл.
    Диалоговое окно указания имени и места сохранения файла
    вызывается при помощи модуля tkinter.filedialog
    """
    save_as = filedialog.asksaveasfilename(
        defaultextension=".json",
        filetypes=[("Json_file", "*.json")],
        initialfile="magic_json.json",
    )
    try:
        with open(save_as, "w", encoding="utf-8") as magic_json:
            raw_json = text_widget.get(1.0, tk.END)
            reorg_json = json.loads(raw_json)
            json.dump(reorg_json, magic_json, ensure_ascii=False, indent=4)
            label_for_text.configure(text="Done!", fg="#00E200")
    except FileNotFoundError:
        msg = str(sys.exc_info()[1])
        mb.showinfo("Directory not selected!", msg)
        label_for_text.configure(text="Listen...", fg=MAIN_FG)
    except json.decoder.JSONDecodeError:
        msg = str(sys.exc_info()[1])
        mb.showerror("Json inserted error", msg)
        label_for_text.configure(text="Error!", fg="red")


def clear_all():
    """Clears text_widget area"""
    text_widget.delete(1.0, tk.END)
    label_for_text.configure(text="Listen...", fg=MAIN_FG)


def paste_clipboard():
    """Pastes the contents of the clipboard into the text_widget area"""
    text_widget.delete(1.0, tk.END)
    buffer_os = window.clipboard_get()
    text_widget.insert(1.0, buffer_os)


def checking_a_previously_used_token():
    """Checking the existence of a previously used token and substituting it"""
    meta_date = os.path.exists("metadate_file")
    if meta_date:
        with open("metadate_file", "r", encoding="utf-8") as metadate_file:
            date_string = metadate_file.read()
            list_of_dates = list(date_string.split(","))
            input_evo_token.delete(0, tk.END)
            input_evo_token.insert(0, list_of_dates[2])


def checking_token_field():
    """Check for a non-empty string in the token field"""
    value = input_evo_token.get()
    if value == "":
        msg = 'Поле "Токен приложения" не заполнено'
        mb.showinfo("Empty field", msg)
        return False
    return True


def open_clndr():
    """Opening a child window"""
    text_widget.delete(1.0, tk.END)
    if checking_token_field() is True:
        clndr_types_date = child_query.clndr.Clndr(
            window, input_evo_token.get(), text_widget, label_for_text
        )
        clndr_types_date.grab_set()
        clndr_types_date.focus_set()
        clndr_types_date.resizable(False, False)
        clndr_types_date.time_handler()


def token_entry_clear():
    """Token entry clear"""
    input_evo_token.delete(0, tk.END)


def open_search_bar(event):
    """Not supported in current version"""
    search_func.Search(window, text_widget)


def decode_without_saving():
    """Perform serialization in the same window"""
    raw_json = text_widget.get(1.0, tk.END)

    try:
        text_widget.delete(1.0, tk.END)
        decoded_json = json.loads(raw_json)
        text_widget.insert(1.0, json.dumps(decoded_json, ensure_ascii=False, indent=4))
    except json.decoder.JSONDecodeError:
        text_widget.insert(1.0, raw_json)
        msg = str(sys.exc_info()[1])
        mb.showerror("Json inserted error", msg)
        label_for_text.configure(text="Error!", fg="red")


# header

label_for_request = Label(
    window, text="Получение документа из облака Эвотор", bg=MAIN_BG, fg=MAIN_FG
)
label_for_request.pack()

main_frame_for_request = tk.Frame(bg=MAIN_BG)
main_frame_for_request.pack(padx=3)

frame_with_label = LabelFrame(
    main_frame_for_request,
    text="Токен приложения",
    bg=MAIN_BG,
    fg=MAIN_FG,
    borderwidth=0,
    width=1,
    font="sans-serif 7",
)
frame_with_label.pack(side=LEFT, pady=3)

input_evo_token = Entry(
    frame_with_label,
    bg=MAIN_FG,
    fg=MAIN_BG,
    bd=0,
    justify=LEFT,
    width=40,
    highlightthickness=0,
)
input_evo_token.pack(side=LEFT)

# memo

label_for_main_text = Label(window, text="Вставьте json в поле", bg=MAIN_BG, fg=MAIN_FG)
label_for_main_text.pack(pady=3)
label_for_memo_text = Label(
    window,
    text="Для вставки CTR+V, раскладка должна быть английская",
    bg=MAIN_BG,
    fg=MAIN_FG,
    font="sans-serif 7",
)
label_for_memo_text.pack()

frame_for_text = tk.Frame(bg=MAIN_BG)

# scroll and text

scroll_v = Scrollbar(frame_for_text)
scroll_v.pack(side=RIGHT, fill=Y)

scroll_h = Scrollbar(frame_for_text, orient=HORIZONTAL)
scroll_h.pack(side=BOTTOM, fill=X)

text_widget = tk.Text(
    frame_for_text,
    borderwidth=0,
    wrap="none",
    font="sans-serif 10",
    yscrollcommand=scroll_v.set,
    xscrollcommand=scroll_h.set,
    width=80,
    height=20,
)
text_widget.pack(ipady=5, fill=BOTH, expand=True)
text_widget.tag_config('key', background='#DEB887', foreground='#FF6347')

scroll_v.config(command=text_widget.yview)
scroll_h.config(command=text_widget.xview)

# footer

frame_for_notifications = tk.Frame(bg=MAIN_BG)

label_for_text = Label(
    frame_for_notifications, text="Listen...", bg=MAIN_BG, fg=MAIN_FG
)
label_for_text.pack()

frame_for_version = tk.Frame(bg=MAIN_BG)

label_for_version = Label(
    master=frame_for_version,
    text=VERSION,
    width=5,
    bg=MAIN_BG,
    fg="#7B7C7F",
    font="sans-serif 7",
)
label_for_version.pack(side=RIGHT, pady=2)

# buttons

token_entry_clear_btn = Button(
    frame_with_label,
    text='❌',
    bg=MAIN_BG,
    fg=MAIN_FG,
    relief="flat",
    command=token_entry_clear,
    activebackground=MAIN_BG,
    activeforeground="red",
    borderwidth=0
)
token_entry_clear_btn.pack(side=LEFT)

tooltip_token_entry_clear_btn_enter = hat.Hover(
    'Очистить поле ввода токена',
    label_for_text,
    BLACK_LIGHT,
    MAIN_FG)

tooltip_token_entry_clear_btn_leave = hat.Hover(
    'Listen...',
    label_for_text,
    MAIN_BG,
    MAIN_FG
)
token_entry_clear_btn.bind('<Enter>', tooltip_token_entry_clear_btn_enter.hover_on)
token_entry_clear_btn.bind('<Leave>', tooltip_token_entry_clear_btn_leave.hover_off)

frame_for_button = tk.Frame(bg=MAIN_BG)

btn_for_request = Button(
    frame_for_button,
    text="Get",
    width=10,
    command=open_clndr,
    bg=MAIN_BG,
    fg=MAIN_FG,
    relief=STYLE,
    activebackground=MAIN_BG,
    activeforeground=MAIN_FG
)
btn_for_request.pack(side=LEFT)

tooltip_btn_for_request_enter = hat.Hover(
    'Открыть окно параметров запроса',
    label_for_text,
    BLACK_LIGHT,
    MAIN_FG)

tooltip_btn_for_request_leave = hat.Hover(
    'Listen...',
    label_for_text,
    MAIN_BG,
    MAIN_FG
)

btn_for_request.bind('<Enter>', tooltip_btn_for_request_enter.hover_on)
btn_for_request.bind('<Leave>', tooltip_btn_for_request_leave.hover_off)

btn_for_paste = Button(
    master=frame_for_button,
    text="Paste",
    width=10,
    command=paste_clipboard,
    bg=MAIN_BG,
    fg=MAIN_FG,
    relief=STYLE,
    activebackground=MAIN_BG,
    activeforeground=MAIN_FG
)
btn_for_paste.pack(side=LEFT)

tooltip_btn_for_paste_enter = hat.Hover(
    'Вставить содержимое буфера обмена',
    label_for_text,
    BLACK_LIGHT,
    MAIN_FG)

tooltip_btn_for_paste_leave = hat.Hover(
    'Listen...',
    label_for_text,
    MAIN_BG,
    MAIN_FG
)

btn_for_paste.bind('<Enter>', tooltip_btn_for_paste_enter.hover_on)
btn_for_paste.bind('<Leave>', tooltip_btn_for_paste_leave.hover_off)

btn_for_reorg = Button(
    master=frame_for_button,
    text="Save as",
    width=10,
    command=cooked_json,
    bg=MAIN_BG,
    fg=MAIN_FG,
    relief=STYLE,
    activebackground=MAIN_BG,
    activeforeground=MAIN_FG
)
btn_for_reorg.pack(side=LEFT)

tooltip_btn_for_reorg_enter = hat.Hover(
    'Открыть диалоговое окно сохранения файла',
    label_for_text,
    BLACK_LIGHT,
    MAIN_FG)

tooltip_btn_for_reorg_leave = hat.Hover(
    'Listen...',
    label_for_text,
    MAIN_BG,
    MAIN_FG
)

btn_for_reorg.bind('<Enter>', tooltip_btn_for_reorg_enter.hover_on)
btn_for_reorg.bind('<Leave>', tooltip_btn_for_reorg_leave.hover_off)

btn_for_clear = Button(
    master=frame_for_button,
    text="Clear",
    width=10,
    command=clear_all,
    bg=MAIN_BG,
    fg=MAIN_FG,
    relief=STYLE,
    activebackground=MAIN_BG,
    activeforeground=MAIN_FG
)
btn_for_clear.pack(side=LEFT)

tooltip_btn_for_clear_enter = hat.Hover(
    'Очистить содержимое поля для текста',
    label_for_text,
    BLACK_LIGHT,
    MAIN_FG)

tooltip_btn_for_clear_leave = hat.Hover(
    'Listen...',
    label_for_text,
    MAIN_BG,
    MAIN_FG
)

btn_for_clear.bind('<Enter>', tooltip_btn_for_clear_enter.hover_on)
btn_for_clear.bind('<Leave>', tooltip_btn_for_clear_leave.hover_off)

btn_for_decode = Button(
    master=frame_for_button,
    text="Decode",
    width=10,
    command=decode_without_saving,
    bg=MAIN_BG,
    fg=MAIN_FG,
    relief=STYLE,
    activebackground=MAIN_BG,
    activeforeground=MAIN_FG
)

btn_for_decode.pack(side=LEFT)

tooltip_btn_for_decode_enter = hat.Hover(
    'Декодирование без сохранения в файл',
    label_for_text,
    BLACK_LIGHT,
    MAIN_FG)

tooltip_btn_for_decode_leave = hat.Hover(
    'Listen...',
    label_for_text,
    MAIN_BG,
    MAIN_FG
)

btn_for_decode.bind('<Enter>', tooltip_btn_for_decode_enter.hover_on)
btn_for_decode.bind('<Leave>', tooltip_btn_for_decode_leave.hover_off)

# pack

frame_for_text.pack(padx=5, ipady=5, fill=BOTH, expand=True)
frame_for_button.pack(pady=5, padx=5, ipady=10)
frame_for_notifications.pack(padx=5, ipady=5)
frame_for_version.pack(fill=X, padx=2)

checking_a_previously_used_token()
text_widget.focus()
window.bind('<Control-f>', open_search_bar)
window.mainloop()
