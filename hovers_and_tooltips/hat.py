class Hover:
    """Вывод подсказки под виджет и подсвечивание выделенной области кнопки

    :param text: текст подсказки
    :type text: str
    :param footer: метка, куда будет вставляться текст (class 'tkinter.Label)
    :param bg: цвет подсветки кнопки
    :type bg: str
    :param fg: цвет текста подсказки
    :type fg: str
    """
    def __init__(self, text, footer, bg, fg) -> None:
        self.text = text
        self.footer = footer
        self.bg = bg
        self.fg = fg

    def hover_on(self, event) -> None:
        """Настройка поведения при наведении курсора на кнопку"""
        event.widget['bg'] = self.bg
        self.footer.configure(text=self.text, fg=self.fg)

    def hover_off(self, event) -> None:
        """Настройка поведения при сдвиге курсора с кнопки"""
        event.widget['bg'] = self.bg
        self.footer.configure(text=self.text, fg=self.fg)
