"""Class for search function"""

import tkinter as tk

death_count = 1


class Search(tk.Toplevel):
    """Класс поиска значений в поле для текста"""
    main_bg = "#282c34"
    main_fg = "#FFFFFF"
    main_widgets_width = 40
    style = "flat"
    BLACK_LIGHT = '#3e4451'

    def search(self, event):
        """Search keywords"""
        key_count = 0
        keyword = self.search_bar.get().split(",")
        if keyword:
            start_index = 1.0
            for key in keyword:
                key_count += 1
                index_item = self.widget.search(key, start_index, tk.END)
                stop_index_item = f'{index_item}+{len(key)}c'
                if self.widget.tag_ranges('key'):
                    self.widget.tag_remove('key', start_index, tk.END)
                self.widget.tag_add('key', index_item, stop_index_item)
                self.widget.see(stop_index_item)
            return key_count
        return key_count

    def __init__(self, parent, widget):
        super().__init__(parent)
        self.geometry("300x50+300+300")
        self["bg"] = self.main_bg
        self.title("Поиск")
        self.widget = widget

        self.frame = tk.Frame(
            self,
            bg=self.main_bg,
        )
        self.frame.pack()

        self.search_bar = tk.Entry(
            self.frame,
            bg=self.main_fg,
            fg=self.main_bg,
            width=self.main_widgets_width,
        )
        self.search_bar.pack(side='left')

        self.button = tk.Button(
            self.frame,
            text="Ок",
            bg=self.main_bg,
            font="sans-serif 10",
            fg=self.main_fg,
            relief=self.style,
            activebackground=self.main_bg,
            activeforeground=self.main_fg,
            command=self.search
        )
        # self.button.pack(side='left')

        self.nav_frame = tk.Frame(
            self,
            bg=self.main_bg,
        )
        self.nav_frame.pack()

        self.nav_label = tk.Label(
            self,
            text=f"Найдено: {death_count}",
            font="sans-serif 7",
            bg=self.main_bg,
            fg=self.main_fg,
        )
        self.nav_label.pack()
        self.search_bar.focus()
        self.bind('<Return>', self.search)
